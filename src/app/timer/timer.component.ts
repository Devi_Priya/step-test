import { Component, OnInit ,Output,EventEmitter,Input} from '@angular/core';
@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.css']
})
export class TimerComponent implements OnInit {
  
  timeObj: any = { "hour": "00", "min": "00", "sec": "00" };
  interval:any;

  constructor() { }
  @Output() fromtimer=new EventEmitter();
  @Input() sTime ={"hour":0,"min":0,"sec":0};

  

  ngOnInit() {console.log(this.sTime);
    this.timerinit(this.sTime.hour,this.sTime.min,this.sTime.sec);
  
  }
  resetTimer()
  {
    clearInterval(this.interval);
  }
  pad(size) {
    var s = String(this.timeObj.sec);
    while (s.length < (size || 2)) {s = "0" + s;}
    return s;
   }
  timerinit(h, m, s) {
    this.timeObj.hour = h;
    this.timeObj.min = m;
    this.timeObj.sec = s;
    this.timerRun();
  }
  timerRun() {
    this.interval = setInterval(() => {
      this.timeObj.sec = this.timeObj.sec - 1;
      this.timeObj.sec = this.pad(2);
      
      if (this.timeObj.sec < 0) {
        this.timeObj.sec = 59;
        this.timeObj.min = this.timeObj.min - 1;
      }
      if (this.timeObj.min < 0) {
        this.timeObj.min = 59;
        this.timeObj.hour = this.timeObj.hour - 1;
      }
      if ((this.timeObj.hour) < 0) {
        clearInterval(this.interval);
        this.timeObj.hour = 0;
        this.timeObj.min = 0;
        this.timeObj.sec = 0;
      }
      this.fromtimer.emit(this.timeObj);
    }, 1000)
  }
}