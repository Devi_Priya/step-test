import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { TestService } from './test.service';
import{FormsModule} from '@angular/forms';
import {Routes,RouterModule} from '@angular/router';


import { AppComponent } from './app.component';
import { GrammartempComponent } from './grammartemp/grammartemp.component';
import { TimerComponent } from './timer/timer.component';
import { IndexComponent } from './index/index.component';
import { FrontpageComponent } from './frontpage/frontpage.component';
import { PauseComponent } from './pause/pause.component';
import { ListeningFrontComponent } from './listening-front/listening-front.component';
import { ListeningtempComponent } from './listeningtemp/listeningtemp.component';
import { ReadingFrontComponent } from './reading-front/reading-front.component';

const Routes: Routes=[
  {path:'listenfront', component:ListeningFrontComponent},
  {path:'listentemp', component:ListeningtempComponent},
  {path:'grammar', component:GrammartempComponent},
  {path:'frontpage',component:FrontpageComponent},
  {path:'readfront', component:ReadingFrontComponent},
  {path:'**',redirectTo:"frontpage",pathMatch:'full'}
  ]



@NgModule({
  declarations: [
    AppComponent,
    GrammartempComponent,
    TimerComponent,
    IndexComponent,
    FrontpageComponent,
    PauseComponent,
    ListeningFrontComponent,
    ListeningtempComponent,
    ReadingFrontComponent,
   
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot(Routes)

  ],
  providers: [TestService],
  bootstrap: [AppComponent]
})
export class AppModule { }
