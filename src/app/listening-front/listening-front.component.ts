import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-listening-front',
  templateUrl: './listening-front.component.html',
  styleUrls: ['./listening-front.component.css']
})
export class ListeningFrontComponent implements OnInit {

  constructor(public route:Router) { }

  ngOnInit() {
  }
next(){
  this.route.navigate(['listentemp'])
}
}
