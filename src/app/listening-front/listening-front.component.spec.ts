import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeningFrontComponent } from './listening-front.component';

describe('ListeningFrontComponent', () => {
  let component: ListeningFrontComponent;
  let fixture: ComponentFixture<ListeningFrontComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeningFrontComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeningFrontComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
