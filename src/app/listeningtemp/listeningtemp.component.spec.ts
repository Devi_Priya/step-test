import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeningtempComponent } from './listeningtemp.component';

describe('ListeningtempComponent', () => {
  let component: ListeningtempComponent;
  let fixture: ComponentFixture<ListeningtempComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeningtempComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeningtempComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
