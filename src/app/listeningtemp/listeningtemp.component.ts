import { Component, OnInit } from '@angular/core';
import { TestService } from '../test.service';
import { Observable } from 'rxjs/Observable';
import { TimerComponent } from '../timer/timer.component';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';

@Component({
  selector: 'app-listeningtemp',
  templateUrl: './listeningtemp.component.html',
  styleUrls: ['./listeningtemp.component.css'],
  providers: [TimerComponent]
})
export class ListeningtempComponent implements OnInit {
  public listenQuestions:any;
  public listenInstructions:any;
  public count:number=0;
  public ex:any={"Qa":[""]};
  public audioURLjson:string;
  public audioURL:HTMLAudioElement;
  public fromstime:any;
  eveHere: any;
  public selectedAns:any;
  public alert:any;
  public questions:any;
  public SelectedIns:any={"instruction":""};
  public timer:any;
  public instructions:any;
  public pause:any;
  public question:any;
  public gottenTime:any;
  public ListenImage:any;
  public id:any;



  constructor(public time: TimerComponent, private ListenQuestion: TestService, public ListenInstruction: TestService, public route: Router) {
    this.fromstime = { "hour": 0, "min": 25, "sec": 10 }; 
   }

  public ngOnInit():void {
    this.ListenQuestion.getlistenQuestion(this.count).subscribe(data => { this.listenQuestions = data; this.ex=data.Ex[0];this.id=this.ex.Qa[0].IM;
      for(let i=0;i<this.ex.Qa.length;i++){
        this.audioURLjson=this.ex.Qa[i].Aud;
        this.audioURL=new Audio(this.audioURLjson);
      }
    console.log(this.listenQuestions) });
    this.ListenInstruction.getlistenInstructions().subscribe(data => { this.listenInstructions = data;this.getInstruction() });
    this.eveHere = { "hour": 0, "min": 0, "sec": 0 };
    this.time.timerRun();

  }
  clickAudio(){
    this.audioURL.play();
  }
  abc(eve) {

    this.eveHere = eve;
    if ((this.eveHere.hour <= 0) && (this.eveHere.min <= 0) && (this.eveHere.sec <= 0)) {

      this.route.navigate(['readfront']);
    }
    console.log(eve);
    let locTime = JSON.parse(localStorage.getItem('Timer'));
    if (locTime.hour <= 0 && locTime.min <= 0 && locTime.sec <= 0) {

    }
    else {
      this.setTime();
      localStorage.setItem('Timer', '{"hour":0,"min":0,"sec":0}');
    }
  }
  TimeReset() {
    //alert();
    this.eveHere.min = 0;
    this.eveHere.sec = 0;
    this.route.navigate(['readfront']);
  }
  onClick() {
    if (this.selectedAns) {
      this.alert = false;
      this.selectedAns = null;
      this.count = this.count + 1;
      if (this.count < 2) {
        this.ListenQuestion.getlistenQuestion(this.count).subscribe(data => { this.listenQuestions = data; this.ex=data.Ex[0]; 
          for(let i=0;i<this.ex.Qa.length;i++){
            this.audioURLjson=this.ex.Qa[i].Aud;
            this.audioURL=new Audio(this.audioURLjson);
          }});
        this.getInstruction();


      }
      else {

        this.TimeReset();

      }
    }
    else {
      this.alert = true;
    }
  }

  grammar(ans) {
    this.selectedAns = ans;
    if (this.selectedAns)
      this.alert = false;
  }

  getInstruction() {console.log(this.id);
    for (var i = 0; i < this.listenInstructions.length; i++) {
      if (this.listenInstructions[i].id == this.id) {
        this.SelectedIns = this.listenInstructions[i];
      }
    }


  }
  pausePlayMainListen(pause) {
    this.pause = pause;
    console.log(this.pause);
  }
  pausePlayListen() {
    this.pause = true;
    let pt = this.eveHere;
    console.log(pt);
    localStorage.setItem('Timer', JSON.stringify(pt));

  }
  SetPauseTime(time) {
    console.log(time);
    this.gottenTime = time;
    this.setTime();
  }
  setTime() {
    let Timer = JSON.parse(localStorage.getItem('Timer'));
    console.log(Timer);
    console.log(this.eveHere);
    this.eveHere.min = Timer.min;
    this.eveHere.sec = Timer.sec;
    console.log(this.eveHere)



  }



}


