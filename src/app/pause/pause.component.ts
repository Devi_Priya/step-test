import { Component, OnInit,Output,EventEmitter,Input} from '@angular/core';
import {GrammartempComponent} from '../grammartemp/grammartemp.component';
//import{ListeningtempComponent} from '../listeningtemp/listeningtemp.component';

@Component({
  selector: 'app-pause',
  templateUrl: './pause.component.html',
  styleUrls: ['./pause.component.css']
})
export class PauseComponent implements OnInit {
public pauseTest:boolean=false;

  constructor(public grammar:GrammartempComponent) { }
  @Output()  pauseTestEvent=new EventEmitter();
  @Input() PauseTime:Object; 
  @Output() PauseTimeEvent=new EventEmitter();
  // @Output()  pauseTestEventListen=new EventEmitter();
  // @Input() PauseTimeListen:Object; 
  // @Output() PauseTimeEventListen=new EventEmitter();

  ngOnInit() {
    
  }
pausePlayMain()
{
 
    this.pauseTest=false;
    this.pauseTestEvent.emit(this.pauseTest);
    //  this.listening.setTime();
    //this.grammar.setTime();
    
}

// pausePlayMainListen()
// {
 
//     this.pauseTest=false;
//     this.pauseTestEventListen.emit(this.pauseTest);
//     this.listen.setTime();
//     console.log(this.listen.setTime());
    
// }
}
