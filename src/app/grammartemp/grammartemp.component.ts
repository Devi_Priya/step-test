import { Component, OnInit } from '@angular/core';
import { TestService } from '../test.service';
import { Observable } from 'rxjs/Observable';
import { TimerComponent } from '../timer/timer.component';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';




@Component({
  selector: 'app-grammartemp',
  templateUrl: './grammartemp.component.html',
  styleUrls: ['./grammartemp.component.css'],
  providers: [TimerComponent]
})
export class GrammartempComponent implements OnInit {
  public questions: any;
  public count: number = 0;
  eveHere: any;
  private gottenTime: any;
  public instructions: any;
  public SelectedIns: any = "<div></div>"
  public selectedAns: any;
  public alert: boolean = false;
  public pause: boolean = false;
  public fromstime: any;


  constructor(public time: TimerComponent, private question: TestService, public instruction: TestService, public route: Router) {
    this.fromstime = { "hour": 0, "min": 10, "sec": 10 };
  }

  public ngOnInit(): void {

    this.question.getQuestion(this.count).subscribe(data => { this.questions = data; });
    this.instruction.getInstructions().subscribe(data => { this.instructions = data; this.getInstruction() });
    this.eveHere = { "hour": 0, "min": 0, "sec": 0 };
    this.time.timerRun();
  }

  abc(eve) {

    this.eveHere = eve;
    if ((this.eveHere.hour <= 0) && (this.eveHere.min <= 0) && (this.eveHere.sec <= 0)) {

      this.route.navigate(['listenfront']);
    }
    console.log(eve);
    let locTime = JSON.parse(localStorage.getItem('Timer'));
    if (locTime.hour <= 0 && locTime.min <= 0 && locTime.sec <= 0) {

    }
    else {
      this.setTime();
      localStorage.setItem('Timer', '{"hour":0,"min":0,"sec":0}');
    }
  }
  TimeReset() {
    //alert();
    this.eveHere.min = 0;
    this.eveHere.sec = 0;
    this.route.navigate(['listenfront']);
  }

  onClick() {
    if (this.selectedAns) {
      this.alert = false;
      this.selectedAns = null;
      this.count = this.count + 1;
      if (this.count < 7) {
        this.question.getQuestion(this.count).subscribe(data => { this.questions = data });
        this.getInstruction();


      }
      else {

        this.TimeReset();

      }
    }
    else {
      this.alert = true;
    }
  }

  grammar(ans) {
    this.selectedAns = ans;
    if (this.selectedAns)
      this.alert = false;
  }

  getInstruction() {
    for (var i = 0; i < this.instructions.length; i++) {
      if (this.instructions[i].id == this.questions[0].IM) {
        this.SelectedIns = this.instructions[i];
        console.log(this.instructions[i]);
      }
    }


  }
  pausePlayMain(pause) {
    this.pause = pause;
    console.log(this.pause);
  }
  pausePlayGrammar() {
    this.pause = true;
    let pt = this.eveHere;
    console.log(pt);
    localStorage.setItem('Timer', JSON.stringify(pt));

  }
  SetPauseTime(time) {
    console.log(time);
    this.gottenTime = time;
    this.setTime();
  }
  setTime() {
    let Timer = JSON.parse(localStorage.getItem('Timer'));
    console.log(Timer);
    console.log(this.eveHere);
    this.eveHere.min = Timer.min;
    this.eveHere.sec = Timer.sec;
    console.log(this.eveHere)



  }



}

