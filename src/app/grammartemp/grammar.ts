export interface IQuestion{
    id: number,
    classid: number,
    section: string,
    question: string,
    answerOptions: string,
    testId:number,
    timer:number
}