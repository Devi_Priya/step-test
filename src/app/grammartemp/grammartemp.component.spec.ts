import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrammartempComponent } from './grammartemp.component';

describe('GrammartempComponent', () => {
  let component: GrammartempComponent;
  let fixture: ComponentFixture<GrammartempComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrammartempComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrammartempComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
