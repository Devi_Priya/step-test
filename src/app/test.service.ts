import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { forEach } from '@angular/router/src/utils/collection';

import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';


@Injectable()
export class TestService {

  

 


  constructor(private http: Http) { }
  getQuestion(param) {
    return this.http.get("./assets/data/grammar"+param+".json").map((res: Response) => {
      return res.json();
    });
  }
  getInstructions(){
    return this.http.get("./assets/data/instructions.json").map((res1: Response) =>
    {return res1.json();
    });
  }

  getlistenQuestion(param1){
    return this.http.get("./assets/data/listening"+param1+".json").map((res2:Response)=>
  {return res2.json();
  });
}

getlistenInstructions(){
  return this.http.get("./assets/data/listeninginstructions.json").map((res3: Response) =>
  {return res3.json();
  });
}

}











 
