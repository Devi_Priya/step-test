import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadingFrontComponent } from './reading-front.component';

describe('ReadingFrontComponent', () => {
  let component: ReadingFrontComponent;
  let fixture: ComponentFixture<ReadingFrontComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadingFrontComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadingFrontComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
